﻿using CotizadorAseguradoras.AbaHogar;
using CotizadorAseguradoras.LaLatino;
using CotizadorAseguradoras.Qualitas;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CotizadorAseguradoras
{
    /// <summary>
    /// Descripción breve de CotizadorWS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class CotizadorWS : System.Web.Services.WebService
    {
        private string pass = "Ahorra2020";
        private string usr = "Ahorra2020";
        private Seguro seguro;
        private string logWSCot;
        private LogCotizacion log = new LogCotizacion();
        private CotizadorQualitas qualitas;
        LaLatino.LatinoCot latino;
        FormatoJsonlLatino jsonLatino = new FormatoJsonlLatino();
        FormatoJsonGenericoBQ jsonGenerico = new FormatoJsonGenericoBQ();
        //--------
        public string seguroNombre = "";
        public string clave = "";
        public string cp = "";
        public string descripcion = "";
        public string descuento = "";
        public string edad = "";
        public string fechaNacimiento = "";
        public string genero = "";
        public string marca = "";
        public int modelo = 0;
        public string movimiento = "";
        public string paquete = "";
        public string servicio = "";
        private string LatinoJson = "";
        private string JsonGenericoBQ = "";
        //Variables Mysql valor
        private string marcasxASeguradoras = "";
        LogCotizacion lg = new LogCotizacion();

        private DatosHogar datosHogar;
        private string IdLogWsHogar;
        LogCotizacionHogar lgH = new LogCotizacionHogar();
        //Pendiente Objeto a la clase para guardar a ka BD

        private AbaHogarCotizador abaCotizador;

        [WebMethod]
        public string CotizadorHogar(string usuario, string pass, string data, string movimiento)
        {
            if (this.pass == pass && this.usr == usuario)
            {
                try
                {
                    string salida = "";
                    try
                    {
                        this.datosHogar = new DatosHogar();
                        this.datosHogar = JsonConvert.DeserializeObject<DatosHogar>(data);
                    }
                    catch (Exception exc)
                    {
                        throw new ArgumentException("Ah habido un error con los datos del JSON " + exc.Message);
                    }
                    if (movimiento == "cotizacion")
                    {
                        //Funcion de insercion al log 
                        IdLogWsHogar = lgH.InsertLog(data);
                        //Aqui iría el despachador pero solo esta ABA :v
                        abaCotizador = new AbaHogarCotizador();
                        salida = abaCotizador.CotizarHogar(datosHogar, IdLogWsHogar);
                        return salida;

                    }
                    else if (movimiento == "emision")
                    {
                        AbaHogarEmision ae = new AbaHogarEmision();
                        string xml = ae.GenerateXmlPersona();
                        salida = ae.registroClientes();
                        return salida;
                    }
                    else
                    {
                        throw new ArgumentException("El movimiento no existe");
                    }
                   
                }
                catch (Exception ex)
                {
                    datosHogar.codigoError = ex.Message;
                    return JsonConvert.SerializeObject(datosHogar);
                }
            }
            return "Error de autentificacion";
        }
    

    [WebMethod]
        public string GetMarcas(string usuario, string password, string aseguradora)
        {
            string marcasJson = lg.GetMarcasMyysql(aseguradora);
            return marcasJson;
        }

        [WebMethod]
        public string GetModelos(string usuario, string password, string aseguradora, string marca)
        {
            string modelosJson = lg.GetModelosMySql(aseguradora,marca); ;
            return modelosJson;
        }

        [WebMethod]
        public string GetDescripcion(string usuario, string password, string aseguradora, string marca, string modelo, string subMarca)
        {
            string descripcionesJson = lg.GetDescripcionesMySql(aseguradora, marca, modelo,subMarca);
            return descripcionesJson;
        }

        [WebMethod]
        public string GetSubMarca(string usuario, string password, string aseguradora, string marca, string modelo)
        {
            string subMarcasJson = lg.GetSubMarcaMySql(aseguradora, marca, modelo);
            return subMarcasJson;
        }

        [WebMethod]
        public string cotizador(string usuario, string pass, string data, string movimiento)
        {
          
            if (this.pass == pass && this.usr == usuario)
            {
                try
                {
                    string salida = "";

                    try
                    {
                        
                        if(movimiento == "cotizacion")
                        {
                            this.seguro = JsonConvert.DeserializeObject<Seguro>(data);
                            JsonFormato listseguros = JsonConvert.DeserializeObject<JsonFormato>(data);
                            seguroNombre = listseguros.Aseguradora;
                            paquete = listseguros.Paquete;
                            descuento = listseguros.Descuento.ToString();
                            descripcion = listseguros.Descripcion;
                            servicio = listseguros.Servicio;
                            marca = listseguros.Marca;
                            modelo = listseguros.Modelo;
                            clave = listseguros.Clave;
                            cp = listseguros.Cp;
                            edad = listseguros.Edad.ToString();
                            genero = listseguros.Genero;
                            movimiento = listseguros.Movimiento;
                            fechaNacimiento = listseguros.FechaNacimiento;

                            switch (seguro.Aseguradora)
                            {
                                case "LATINO":

                                    //-----
                                    LatinoJson = jsonLatino.JsonLatino(seguroNombre, clave, cp, descripcion, descuento, edad,
                                        fechaNacimiento, genero, marca, modelo, paquete, servicio);

                                    this.seguro = new Seguro();
                                    this.seguro = JsonConvert.DeserializeObject<Seguro>(LatinoJson);

                                    break;

                                case "QUALITAS":
                                    JsonGenericoBQ = jsonGenerico.JsonGenericoQB(descripcion, seguroNombre, clave, cp, descuento, edad,
                                     fechaNacimiento, genero, marca, modelo, paquete, servicio);

                                    this.seguro = new Seguro();
                                    this.seguro = JsonConvert.DeserializeObject<Seguro>(JsonGenericoBQ);

                                    break;
                            }
                        }
                        else
                        {
                            this.seguro = new Seguro();
                            this.seguro = JsonConvert.DeserializeObject<Seguro>(data);
                        }

                    }
                    catch (Exception exc)
                    {
                        throw new ArgumentException("Ah habido un error con los datos del JSON " + exc.Message);
                    }

                    if (movimiento == "cotizacion")
                        logWSCot = log.InsertLog(data, seguro.Aseguradora, seguro.Vehiculo.Marca, seguro.Vehiculo.Modelo.ToString(), seguro.Vehiculo.Descripcion, seguro.Vehiculo.Clave); // aqui va la funcion de insercion en el log
                    else if (movimiento == "emision" || movimiento == "Emision")
                        logWSCot = log.InsertLogEmision(data, seguro.Aseguradora, seguro.Vehiculo.Marca, seguro.Vehiculo.Modelo.ToString(), seguro.Vehiculo.Descripcion, seguro.Vehiculo.Clave); // aqui va la funcion de insercion en el log
                    else
                    {
                        throw new ArgumentException("Ah habido un error con los datos del movimiento");
                    }
                    switch (seguro.Aseguradora)
                    {
                        case "BANORTE":
                            {
                                if (movimiento == "cotizacion")
                                {
                                //    banorte = new BanorteCot();
                                //    salida = banorte.BanorteCotizacionResponse(seguro, logWSCot);

                                }
                                else
                                {
                                    //Funcion de emision  salida = banorte.BanorteCotizacionResponse(seguro, logWSCot);
                                }
                                break;
                            }
                        case "QUALITAS":
                            {
                                if (movimiento == "cotizacion")

                                {
                                    qualitas = new CotizadorQualitas(); // Instancia para el objeto de la clase qualitas
                                    salida =  //Prueba para inserción de log
                                    //qualitas = new CotizadorQualitas(); // Instancia para el objeto de la clase qualitas
                                    salida = qualitas.CotizaQualitas(seguro, logWSCot);


                                }
                                else
                                {
                                    //Funcion de emision  
                                    
                                }
                                break;
                            }
                        case "LATINO":
                            {
                                if (movimiento == "cotizacion")
                                {

                                    latino = new LatinoCot();
                                     salida = latino.CotizacionLatino(data,seguro, logWSCot, movimiento, 
                                      seguro.Cliente.Direccion.Colonia,
                                      seguro.Cliente.Nombre,
                                      seguro.Cliente.ApellidoPat,
                                      seguro.Cliente.ApellidoMat,
                                      seguro.Cliente.FechaNacimiento,
                                      seguro.Cliente.RFC,
                                      seguro.Cliente.Direccion.Calle,
                                      seguro.Cliente.Direccion.NoInt,
                                      seguro.Cliente.Direccion.NoExt,
                                      seguro.Vehiculo.NoSerie,
                                      seguro.Vehiculo.NoMotor,
                                      seguro.Vehiculo.NoPlacas,
                                      seguro.Vehiculo.Clave);                                }
                                else
                                {
                                    //Funcion de emision 
                                    //Datos de prueba
                                    seguro.Vehiculo.Clave = "ControlV";
                                    seguro.Vehiculo.NoSerie = "8350274ETF";
                                    seguro.Vehiculo.NoMotor = "12234321";
                                    seguro.Vehiculo.NoPlacas = "MEXDF123";
                                    latino = new LatinoCot();
                                    salida = latino.CotizacionLatino(data,seguro, logWSCot, movimiento, 
                                      seguro.Cliente.Direccion.Colonia,
                                      seguro.Cliente.Nombre,
                                      seguro.Cliente.ApellidoPat,
                                      seguro.Cliente.ApellidoMat,
                                      seguro.Cliente.FechaNacimiento,
                                      seguro.Cliente.RFC,
                                      seguro.Cliente.Direccion.Calle,
                                      seguro.Cliente.Direccion.NoInt,
                                      seguro.Cliente.Direccion.NoExt,
                                      seguro.Vehiculo.NoSerie,
                                      seguro.Vehiculo.NoMotor,
                                      seguro.Vehiculo.NoPlacas,
                                      seguro.Vehiculo.Clave);
                                }
                                break;
                            }

                    }

                    //Aqui va la funcion de validacion Validacion = ValCotizacion(ObjData);
                    return salida;
                }
                catch (Exception exc)
                {
                    seguro.codigoError = exc.Message;
                    return JsonConvert.SerializeObject(seguro);
                }
            }
            else
            {
                return "Error de autenticacion";
            }
        }

      
    }
}
