﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CotizadorAseguradoras
{
    /// <summary>
    /// Descripción breve de ServiceLaLatino
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class ServiceLaLatino : System.Web.Services.WebService
    {

        private LogCotizacion lg = new LogCotizacion();


        [WebMethod]
        public string GetSubMarcaLatino(string marca, string modelo, string descripcion)
        {
            string claveSubMarca = lg.GetSubMarcaLatino(marca, modelo, descripcion);
            return claveSubMarca;
        }

        [WebMethod]
        public string GetDescripcionLatino(string marca, string submarca, string modelo, string descripcion)
        {
            string Descipcion = lg.GetDescripcion(marca, submarca, modelo, descripcion);
            return Descipcion;
        }

        [WebMethod]
        public string GetClaveDescripcionLatino(string clave_SubMarca, string cLave_Marca, string descripcion, string año)
        {
            string clave_Descripcion = lg.GetClaveDescripcionLatino(clave_SubMarca, cLave_Marca, descripcion, año);
            return clave_Descripcion;
        }

    }
}
