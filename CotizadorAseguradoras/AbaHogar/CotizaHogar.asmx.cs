﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CotizadorAseguradoras.AbaHogar
{
    /// <summary>
    /// Descripción breve de CotizaHogar
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class CotizaHogar : System.Web.Services.WebService
    {
        private string usr = "";
        private string pass = "";
        private DatosHogar datosHogar;
        private string IdLogWsHogar;
        LogCotizacionHogar lgH = new LogCotizacionHogar();
        //Pendiente Objeto a la clase para guardar a ka BD

        private AbaHogarCotizador abaCotizador;
       
        [WebMethod]
        public string CotizadorHogar(string usuario,string pass, string data, string movimiento)
        {
            if (this.pass == pass && this.usr == usuario)
            {
                try
                {
                    string salida = "";
                    try
                    {
                        this.datosHogar = new DatosHogar();
                        this.datosHogar = JsonConvert.DeserializeObject<DatosHogar>(data);
                    }
                    catch (Exception exc)
                    {
                        throw new ArgumentException("Ah habido un error con los datos del JSON " + exc.Message);
                    }
                    if (movimiento == "cotizacion")
                    {
                        //Funcion de insercion al log 
                       IdLogWsHogar= lgH.InsertLog(data);

                    }else if (movimiento == "emision")
                    {
                        //Funcion del log a emision

                    }
                    else
                    {
                        throw new ArgumentException("El movimiento no existe");
                    }
                    //Aqui iría el despachador pero solo esta ABA :v
                    abaCotizador = new AbaHogarCotizador();
                     salida = abaCotizador.CotizarHogar(datosHogar,IdLogWsHogar);
                    return salida;
                }
                catch (Exception ex)
                {
                    datosHogar.codigoError = ex.Message;
                    return JsonConvert.SerializeObject(datosHogar);
                }
            }
            return "Error de autentificacion";
        }
    }
}
