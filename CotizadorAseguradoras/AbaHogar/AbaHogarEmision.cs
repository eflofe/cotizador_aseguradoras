﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace CotizadorAseguradoras.AbaHogar
{
    public class AbaHogarEmision
    {
        string xmlPersona = "";
        

        public string GenerateXmlPersona()
        {
            XmlDocument document = new XmlDocument();
           // Nodo Principal del XML
            XmlNode rootNode = document.CreateElement("XML");
            document.AppendChild(rootNode);

            //Nodo DP
            XmlNode dpNode = document.CreateElement("DP");
           

            //Nodo TP
            XmlNode TpNode = document.CreateElement("TP");
            TpNode.InnerText = "0";
            //Fin del nodo TP

            //Nodo FISICA
            XmlNode fisica = document.CreateElement("FISICA");
            XmlNode rfc = document.CreateElement("RFC");
            rfc.InnerText = "ZUMA880119"; //ejemplo
            XmlNode hcve = document.CreateElement("HCVE");
            hcve.InnerText = "II2"; //ejemplo
            XmlNode pnom = document.CreateElement("PNOM");
            pnom.InnerText = "Andrés";//ejemplo
            XmlNode snom = document.CreateElement("SNOM");
            snom.InnerText = "";//ejemplo
            XmlNode app = document.CreateElement("APP");
            app.InnerText = "Zúñiga";
            XmlNode apm = document.CreateElement("APM");
            apm.InnerText = "Méndez";
            XmlNode sexo = document.CreateElement("SEXO");
            sexo.InnerText = "0";
            XmlNode edoC = document.CreateElement("EDOCIVIL");
            edoC.InnerText = "2";
            //Fin nodo FISICA

            //Nodo Domicilio
            XmlNode domicilio = document.CreateElement("DOMICILIO");
            XmlNode tipoDir = document.CreateElement("TIPODIR");
            tipoDir.InnerText = "1";
            XmlNode calle = document.CreateElement("CALLE");
            calle.InnerText = "Calzada San Isidro";
            XmlNode numext = document.CreateElement("NUMEXT");
            numext.InnerText = "712";
            XmlNode numint = document.CreateElement("NUMINT");
            numint.InnerText = "";
            XmlNode col = document.CreateElement("COL");
            col.InnerText = "San Pedro Xalpa";
            XmlNode cp = document.CreateElement("CP");
            cp.InnerText = "02710";
            XmlNode pob = document.CreateElement("POB");
            pob.InnerText = "San nicolas";
            //fin nodo domicilio

            //Nodo Telefono
            XmlNode telefono = document.CreateElement("TELEFONO");
            XmlNode lada = document.CreateElement("LADA");
            lada.InnerText = "55";
            XmlNode numero = document.CreateElement("NUMERO");
            numero.InnerText = "72620313";
            //Fin nodo Telefono

            //Nodo CELULAR
            XmlNode celular = document.CreateElement("CELULAR");
            XmlNode ladaCel = document.CreateElement("LADA");
            ladaCel.InnerText = "55";
            XmlNode numeroCel = document.CreateElement("NUMERO");
            numeroCel.InnerText = "14199304";
            //Fin nodo CELULAR

            //nodo correo
            XmlNode correo = document.CreateElement("CORREO");
            correo.InnerText = "andres@brokfy.com";
            //fin nodo correo

            //Fin del nodo DP

            //Grupo del nodo Fisica
            fisica.AppendChild(rfc);
            fisica.AppendChild(hcve);
            fisica.AppendChild(pnom);
            fisica.AppendChild(snom);
            fisica.AppendChild(app);
            fisica.AppendChild(apm);
            fisica.AppendChild(sexo);
            fisica.AppendChild(edoC);
            //Fin del grupo FIsica

            //grupo Nodo Domicilio
            domicilio.AppendChild(tipoDir);
            domicilio.AppendChild(calle);
            domicilio.AppendChild(numext);
            domicilio.AppendChild(numint);
            domicilio.AppendChild(col);
            domicilio.AppendChild(cp);
            domicilio.AppendChild(pob);
            //fin del grupo nodo domicilio

            //grupo del nodo telefono
            telefono.AppendChild(lada);
            telefono.AppendChild(numero);
            //fin del grupo del nodo telefono

            //grupo del nodo celular
            celular.AppendChild(ladaCel);
            celular.AppendChild(numeroCel);
            //fin del grupo nodo celular

            dpNode.AppendChild(TpNode);
            dpNode.AppendChild(fisica);
            dpNode.AppendChild(domicilio);
            dpNode.AppendChild(telefono);
            dpNode.AppendChild(celular);
            

            rootNode.AppendChild(dpNode);
            xmlPersona = document.OuterXml;

            return xmlPersona;
        }

        public string registroClientes()
        {
            RegistroHogar.PCRegistroClient client = new RegistroHogar.PCRegistroClient();
            RegistroHogar.Salida salida = new RegistroHogar.Salida();
            
            RegistroHogar.Token token = new RegistroHogar.Token();
            RegistroHogar.Entrada entrada = new RegistroHogar.Entrada();

            token.usuario = "wsdiversoqa";
            token.password = "Seguro123*";

            entrada.Token = token;
            string entradaXml = xmlPersona ;
            string salidas=salida.strSalida= client.ConsultaRegistraPersona(token,entradaXml);
            return salidas;
        }
    }
  
}