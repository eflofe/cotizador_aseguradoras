﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VistaLatinoEmision.aspx.cs" Inherits="CotizadorAseguradoras.LaLatino.VistaLatinoEmision" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
      <style>
   
        form {
    /* Sólo para centrar el formulario a la página */
    margin: 0 auto;
    width: 400px;
    /* Para ver el borde del formulario */
    padding: 1em;
    border: 1px solid #CCC;
    border-radius: 1em;
}
        form div + div {
    margin-top: 1em;
}
        label {
    /* Para asegurarse que todos los labels tienen el mismo tamaño y están alineados correctamente */
    display: inline-block;
    width: 120px;
    text-align: right;
}
    </style>
    <form id="form1" runat="server">
        <h3>Datos persona</h3>
        <div>
            <asp:Label runat ="server" ID="lblNombre">Nombre</asp:Label>
            <asp:TextBox runat="server" ID="txtNombre"></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" ID="lblaPaterno">Apellido Paterno</asp:Label>
            <asp:TextBox runat="server" ID="txtaPaterno"></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" ID="lblaMaterno">Apellido Materno</asp:Label>
            <asp:TextBox runat="server" ID="TxtMaterno"></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" ID="lblFnacimienti">Fecha de nacimiento</asp:Label>
            <asp:TextBox runat="server" ID="txtFnacimiento"></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" ID="lblcurp">RFC</asp:Label>
            <asp:TextBox runat="server" ID="txtRfc"></asp:TextBox>
        </div>
        <h3>Datos dirección</h3>
        <div>
            <asp:Label runat="server" ID="lblMunicipio">Municipio/estado</asp:Label>
            <asp:TextBox runat="server" ID="txtMunicipio"></asp:TextBox>
        </div>
       <div>
            <asp:DropDownList runat="server" ID="DropColonia" OnSelectedIndexChanged="Dropcolonia_SelectedIndexChanged" AutoPostBack="true" >
            <asp:ListItem Value="0">Seleccione una colonia </asp:ListItem>
            <asp:ListItem Value="1">'NINGUNO'</asp:ListItem>
            </asp:DropDownList>
       </div>
      <div>
            <asp:Label runat="server" ID="lblCalle">Calle</asp:Label>
            <asp:TextBox runat="server" ID="txtCalle" ></asp:TextBox>
      
      </div>
        <div>
            <asp:Label runat="server" ID="txtExterior">NoExterior</asp:Label>
            <asp:TextBox runat="server" ></asp:TextBox>
           
        </div>
        <div>
            <asp:Label runat="server" ID="lblNointerior">Nointerior</asp:Label>
            <asp:TextBox runat="server" ID="txtInterior" ></asp:TextBox>
        </div>

        <h3>Datos del coche</h3>
        <div>
            <asp:Label runat="server" ID="Llbdescripción">Descripción del vehiculo</asp:Label>
            <asp:TextBox runat="server" ID="TxtDescripcionCoche"></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" ID="LblNoSerie">No.Serie</asp:Label>
            <asp:TextBox runat="server" ID="TxtNoSerie" ></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" ID="LblNoMotor">No. motor</asp:Label>
            <asp:TextBox runat="server" ID="TxtNoMotor" ></asp:TextBox>
        </div>
        <div>
            <asp:Label runat="server" ID="LblNplacas">No. placas</asp:Label>
            <asp:TextBox runat="server" ID="TxtNplacas" ></asp:TextBox>
        </div>
        <div>
           <asp:Label runat="server" ID="lblNumeroC">Numero de ControlVehicular</asp:Label>
           <asp:TextBox runat="server" ID="TxtNumeroC" ></asp:TextBox>
         </div>
        <h3>Datos de pago</h3>
        <div>
            <asp:CheckBox runat="server" ID="chekVisa" text="Visa"/>
            <asp:CheckBox runat="server" ID="CheckMaster" text="MasterCard"/>
            <asp:CheckBox runat="server" ID="CheckAmericanExpress" text="AmericanExpress"/>

            
        </div>
        <div>
            <asp:Label runat="server" ID="LblTitular">Titular de la tarjeta</asp:Label>
                        <asp:TextBox runat="server" ID="TxtTitular"></asp:TextBox>

        </div>
        <div>
        </div>
        <div>
                        <asp:Label runat="server">Banco</asp:Label>
            <asp:DropDownList runat="server" ID="DropBanco" OnSelectedIndexChanged="DropBanco_SelectedIndexChanged" AutoPostBack="true" >
           <asp:ListItem Value="0">Seleccione un banco </asp:ListItem>
            <asp:ListItem Value="1">BANORTE</asp:ListItem>
             <asp:ListItem Value="2">BANCOMEX</asp:ListItem>
             <asp:ListItem Value="3">AFIRME</asp:ListItem>
            </asp:DropDownList>

        </div>
        <div>
                        <asp:Label runat="server" ID="LblMetodoPago">Metodo de pago</asp:Label>
               <asp:DropDownList runat="server" ID="DropMetodoPago" OnSelectedIndexChanged="DropMetodoPago_SelectedIndexChanged" AutoPostBack="true" >
           <asp:ListItem Value="0">Seleccione método de pago </asp:ListItem>
            <asp:ListItem Value="1">Credito</asp:ListItem>
             <asp:ListItem Value="2">Debito</asp:ListItem>
            </asp:DropDownList>

        </div>
        <div>
                        <asp:Label runat ="server">Numero de tarjeta</asp:Label>
            <asp:TextBox runat="server" ID="TxtNumTarjeta" ></asp:TextBox>

        </div>
        <div>
            <asp:Label runat="server" ID="Label1">Mes de ex</asp:Label>
            <asp:TextBox runat="server" ID="TxtmesEx" ></asp:TextBox>

        </div>
        <div>
                        <asp:Label runat="server" ID="LblañoEx">Año de ex</asp:Label>
            <asp:TextBox runat="server" ID="TxtañoEx" ></asp:TextBox>

        </div>
        <div id="form4" class="ContenedorPago">
         
            <asp:Label runat="server">Código de seguridad</asp:Label>
            <asp:TextBox runat="server" ID="TxtCodigoSegurida"></asp:TextBox>
        </div>


        <asp:Button runat="server"  Text="Emitir Json poliza" OnClick="BtnCargarJson" ID="btnEmision"/>
        <br />
        <br />

        <asp:Label runat="server" ID="lblJason"></asp:Label>

        <asp:Label runat="server" ID="lblcolonia" Visible="true" ></asp:Label>
        <asp:Label runat="server" ID="lblBanco" Visible="true"></asp:Label>
        <asp:Label runat="server" ID="lblMetodo" Visible="true"></asp:Label>
    </form>
</body>
</html>
