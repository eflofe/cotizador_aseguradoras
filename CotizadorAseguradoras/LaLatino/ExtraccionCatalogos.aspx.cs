﻿using CotizadorAseguradoras.LatinoWS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CotizadorAseguradoras.LaLatino
{
    public partial class ExtraccionCatalogos : System.Web.UI.Page
    {
        protected static string content;
        protected static bool inProcess = false;
        protected static bool processComplete = false;
        protected static string processCompleteMsg = "Finished Processing All Records.";
        private static DateTime time;
        //private delegate void SafeCallDelegate(string text);
        

        private static Semaphore _pool;

        private long usr = 10041;
        private string pass = "CD56JH34ED23MRVC92";
        private string usrlog = "9257";
        private string usrpss = "L4t1nO9257";


  
     
        
        private List<String> errores;
        private List<string> track;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEx_Click(object sender, EventArgs e)
        {
           
            errores = new List<String>();
            track = new List<string>();

            Timer1.Enabled = true;
            content = "";
            Button1.Enabled = false;
            time = DateTime.Now;
            

            if (_pool != null)
                _pool.Dispose();

            Thread work = new Thread(() => Cosas());
            work.Start();
        }

        public void Cosas()
        {
            try
            {
                inProcess = true;
                LatinoWS.CotizadorLatinoClient latinoClient = new LatinoWS.CotizadorLatinoClient();
                LatinoWS.DatosRequeridos datosRequeridos = new LatinoWS.DatosRequeridos();
                LatinoWS.Credencial credencial = new LatinoWS.Credencial();
                credencial.IdApp = this.usr;
                credencial.PassApp = this.pass;
                credencial.ClaveUsuario = this.usrlog;
                credencial.Password = this.usrpss;

                LatinoWS.Auto auto = new LatinoWS.Auto();
                auto.ClaveProducto = "1";
                auto.Tarifa = "1704";
                auto.TipoVehiculo = "AU";
                auto.ClavePerfil = "36013";
                auto.ClaveMarca = "";
                auto.ClaveSubMarca = "";
                auto.ClaveVehiculo = "";
                auto.NumeroSerieAuto = "";
                auto.SerieValida = false;
                auto.NumeroPlacas = "";
                auto.NumeroMotor = "";

                LatinoWS.CaracteristicasCotizacion caracteristicasCotizacion = new LatinoWS.CaracteristicasCotizacion();
                caracteristicasCotizacion.ClaveEstado = "";
                caracteristicasCotizacion.ClavePaquete = "";
                caracteristicasCotizacion.ClaveVigencia = "";
                caracteristicasCotizacion.ClaveServicio = "";
                caracteristicasCotizacion.ClaveDescuento = "";
                caracteristicasCotizacion.ClaveAgente = "";
                caracteristicasCotizacion.ClaveFormaPago = "";
                datosRequeridos.datosAuto = auto;
                datosRequeridos.credenciales = credencial;
                datosRequeridos.caracteristicasCotizacion = caracteristicasCotizacion;
                getCatalogos(latinoClient,datosRequeridos);
                if (errores.Count == 0 && getCatalogos(latinoClient,datosRequeridos))
                {

                }
                processComplete = true;
                content = processCompleteMsg + System.Environment.NewLine + content;
            }
            catch (Exception ex)
            {
                content = ex.Message + " Main Function " + System.Environment.NewLine;
                errores.Add(ex.Message + " |Main Function|");
            }
           
        }

        private Boolean getCatalogos(CotizadorLatinoClient latino, DatosRequeridos datos)
        {
            try
            {
                string estados = "";
                string paquetes = "";
                string forma = "";
                string servicio = "";
                string tipo = "";
                string cobertura = "";
                datos.datosAuto.ClaveMarca = "ACURA";
                datos.datosAuto.ClaveSubMarca = "RDX";
                datos.datosAuto.ClaveModelo = "2019";
                datos.caracteristicasCotizacion.ClaveEstado = "15";
                datos.datosAuto.ClaveVehiculo = "M00100011";
                datos.caracteristicasCotizacion.ClavePaquete = "1";


                LatinoWS.ListCoberturas coberturas = latino.ObtenerCoberturas(datos);
                LatinoWS.ListCatalogos catalogos = latino.ObtenerCatalogos(datos);
                foreach (LatinoWS.Estado estado in catalogos.Estados)
                {
                    estados += estado.Clave + "|" + estado.Descripcion + "<br>";
                }
                foreach (LatinoWS.Paquete paquete in catalogos.Paquetes)
                {
                    paquetes += paquete.Clave + "|" + paquete.Descripcion + "<br>";
                }
                foreach (LatinoWS.FormaPago fp in catalogos.FormasPagos)
                {
                    forma += fp.Clave + "|" + fp.Descripcion + "<br>";
                }
                foreach (LatinoWS.Servicio servicios in catalogos.Servicios)
                {
                    servicio += servicios.Clave + "|" + servicios.Descripcion + "<br>";
                }
                foreach (LatinoWS.TipoVigencia tipos in catalogos.TiposVigencias)
                {
                    tipo += tipos.Clave + "|" + tipos.Descripcion + "<br>";
                }
                foreach (LatinoWS.Coberturas cobe in coberturas.Coberturas)
                {
                    cobertura += cobe.AmparadaInicio + "|" + cobe.Clave + "|" + cobe.Descripcion + "|" + cobe.Opcional + "|" + cobe.PrimaNeta + "<br>";
                }
                CrearTxt(estados, "LatinoEstados");
                CrearTxt(paquetes, "LatinoPaquetes");
                CrearTxt(forma, "LatinoForma");
                CrearTxt(servicio, "LatinoServicio");
                CrearTxt(tipo, "LatinoTipos");
                CrearTxt(cobertura, "LatinoCoberturas");
                return true;
            }
            catch(Exception ex){
                content = ex.Message + " Main Function " + System.Environment.NewLine;
                errores.Add(ex.Message + " |Main Function|");
                return false;
            }
        }

        // ----------------------------Funciones-------------------------------
        public String CrearTxt(String salida, String name)
        {
            try
            {
                FileStream file = File.Create(@"C:\Users\HP\Desktop\" + name + ".txt");
                Byte[] info = new UTF8Encoding(true).GetBytes(salida);
                file.Write(info, 0, info.Length);
                file.Close();
                return "ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            if (inProcess)
            {
                lbl1.Text = (DateTime.Now - time).ToString();
                TextBox1.Text = content;
            }
            if (processComplete && content.Contains(processCompleteMsg)) //has final message been set?
            {
                Timer1.Enabled = false;
                inProcess = false;
                Button1.Enabled = true;
                if (_pool != null)
                    _pool.Dispose();
            }
           
                Button1.Enabled = true;
                processComplete = false;
                if (_pool != null)
                    _pool.Dispose();

            
        }
    }
}