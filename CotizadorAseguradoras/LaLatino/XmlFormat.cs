﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace CotizadorAseguradoras.LaLatino
{
    public class XmlFormat
    {
        private long usr = 10041;
        private string pass = "CD56JH34ED23MRVC92";
        private string usrlog = "9257";
        private string usrpss = "L4t1nO9257";
        private LogCotizacion log = new LogCotizacion();
        private string url = "http://schemas.datacontract.org/2004/07/WCFModelo.Cotizador";
        private string urlCatalogos = "http://schemas.datacontract.org/2004/07/WCFModelo.Catalogos";
        private string envelope = "http://schemas.xmlsoap.org/soap/envelope/";
        private string tempuri = "http://tempuri.org/";

        public string GenerateXml(Seguro seguro, string idEdo, string Paquete, string servicio)
        {
            string xml = "";
            XmlDocument doc = new XmlDocument();
            //Atributos catalogos
            XmlAttribute urlCatalogo = doc.CreateAttribute("xmlns");
            XmlAttribute urlCotizar = doc.CreateAttribute("xmlns");
            XmlAttribute urlCredencial = doc.CreateAttribute("xmlns");
            XmlAttribute urlIdApp = doc.CreateAttribute("xmlns");
            XmlAttribute urlPassApp = doc.CreateAttribute("xmlns");
            XmlAttribute urlClaveUsuario = doc.CreateAttribute("xmlns");
            XmlAttribute urlPassword = doc.CreateAttribute("xmlns");
            //------
            XmlAttribute urlEstado = doc.CreateAttribute("xmlns");
            XmlAttribute urlPaquete = doc.CreateAttribute("xmlns");
            XmlAttribute urlVigencia = doc.CreateAttribute("xmlns");
            XmlAttribute urlServicio = doc.CreateAttribute("xmlns");
            XmlAttribute urlDescuento = doc.CreateAttribute("xmlns");
            XmlAttribute urlAgente = doc.CreateAttribute("xmlns");
            XmlAttribute urlFormaPago = doc.CreateAttribute("xmlns");
            //-----
            XmlAttribute urlClaveProducto = doc.CreateAttribute("xmlns");
            XmlAttribute urlTarifa = doc.CreateAttribute("xmlns");
            XmlAttribute urlTipoVehiculo = doc.CreateAttribute("xmlns");
            XmlAttribute urlClavePerfil = doc.CreateAttribute("xmlns");
            XmlAttribute urlClaveModelo = doc.CreateAttribute("xmlns");
            XmlAttribute urlMarca = doc.CreateAttribute("xmlns");
            XmlAttribute urlSubMarca = doc.CreateAttribute("xmlns");
            XmlAttribute urlClaveVehiculo = doc.CreateAttribute("xmlns");
            XmlAttribute urlNumSerieAuto = doc.CreateAttribute("xmlns");
            XmlAttribute urlSerieValida = doc.CreateAttribute("xmlns");
            XmlAttribute urlNumPlacas = doc.CreateAttribute("xmlns");
            XmlAttribute urlNumeroMotor = doc.CreateAttribute("xmlns");
            XmlAttribute urlControlVehicular = doc.CreateAttribute("xmlns");
            XmlAttribute urlConductor = doc.CreateAttribute("xmlns");
            XmlAttribute urlBeneficiario = doc.CreateAttribute("xmlns");
            //----
           
            //Atributos inicio
            XmlAttribute xmlnsTempuri = doc.CreateAttribute("xmlns");
            XmlAttribute xmlnsevelope = doc.CreateAttribute("xmlns");
            //atributos cotiza
            XmlAttribute urlDatosAuto = doc.CreateAttribute("xmlns");
            XmlAttribute urlcaracteristicas = doc.CreateAttribute("xmlns");
            XmlAttribute urlValorCon = doc.CreateAttribute("xmlns");
            XmlAttribute urlFechaFact = doc.CreateAttribute("xmlns");
            XmlAttribute urlValorFactura = doc.CreateAttribute("xmlns");
            XmlAttribute urlCoberturaAmpa = doc.CreateAttribute("xmlns");

            //valor catalogos
            urlIdApp.Value = urlCatalogos;
            urlPassApp.Value = urlCatalogos;
            urlClaveUsuario.Value = urlCatalogos;
            urlPassword.Value = urlCatalogos;
            //-----
            urlEstado.Value = urlCatalogos;
            urlPaquete.Value = urlCatalogos;
            urlVigencia.Value = urlCatalogos;
            urlServicio.Value = urlCatalogos;
            urlDescuento.Value = urlCatalogos;
            urlAgente.Value = urlCatalogos;
            urlFormaPago.Value = urlCatalogos;
            //----
            urlClaveProducto.Value = urlCatalogos;
            urlTarifa.Value = urlCatalogos;
            urlTipoVehiculo.Value = urlCatalogos;
            urlClavePerfil.Value = urlCatalogos;
            urlClaveModelo.Value = urlCatalogos;
            urlMarca.Value = urlCatalogos;
            urlSubMarca.Value = urlCatalogos;
            urlClaveVehiculo.Value = urlCatalogos;
            urlNumSerieAuto.Value = urlCatalogos;
            urlSerieValida.Value = urlCatalogos;
            urlNumPlacas.Value = urlCatalogos;
            urlNumeroMotor.Value = urlCatalogos;
            urlControlVehicular.Value = urlCatalogos;
            urlConductor.Value = urlCatalogos;
            urlBeneficiario.Value = urlCatalogos;
            //-------

            //valor cotizar
            urlCredencial.Value = url;
            urlcaracteristicas.Value = url;
            urlDatosAuto.Value = url;
            urlValorCon.Value = url;
            urlFechaFact.Value = url;
            urlValorFactura.Value = url;
            urlCoberturaAmpa.Value = url;


            //valor atributos inicio
            xmlnsTempuri.Value = tempuri;
            xmlnsevelope.Value = envelope;

            //Nodo evelope
            XmlNode evelope = doc.CreateElement("Envelope");
            evelope.Attributes.Append(xmlnsevelope);
            doc.AppendChild(evelope);
            //Nodo body
            XmlNode bodyNode = doc.CreateElement("Body");
            //Nodo cotizar
            XmlNode rootNode = doc.CreateElement("Cotizar");
            rootNode.Attributes.Append(xmlnsTempuri);

            //Nodo datosCotizar
            XmlNode datosCotizar = doc.CreateElement("datosCotizar");
            //Fin del nodo datosCotizar
            //inicio nodo credenciales
            XmlNode credenciales = doc.CreateElement("credenciales");
            credenciales.Attributes.Append(urlCredencial);
            XmlNode IdApp = doc.CreateElement("IdApp");
            IdApp.InnerText = "10041";
            IdApp.Attributes.SetNamedItem(urlIdApp);
            XmlNode PassApp = doc.CreateElement("PassApp");
            PassApp.Attributes.Append(urlPassApp);
            PassApp.InnerText = "CD56JH34ED23MRVC92";
            XmlNode claveUsuario = doc.CreateElement("ClaveUsuario");
            claveUsuario.Attributes.Append(urlClaveUsuario);
            claveUsuario.InnerText = "9257";
            XmlNode Password = doc.CreateElement("Password");
            Password.Attributes.Append(urlPassword);
            Password.InnerText = "L4t1nO9257";
            //Fin modulo credenciales

            //inicio nodo caracteristicasCotizacion
            XmlNode caracteristicasCotizacion = doc.CreateElement("caracteristicasCotizacion");
            caracteristicasCotizacion.Attributes.Append(urlcaracteristicas);
            XmlNode claveEstado = doc.CreateElement("ClaveEstado");
            claveEstado.Attributes.Append(urlEstado);
            claveEstado.InnerText = "25";
            XmlNode clavePaquete = doc.CreateElement("ClavePaquete");
            clavePaquete.Attributes.Append(urlPaquete);
            clavePaquete.InnerText = Paquete;
            XmlNode clavevigencia = doc.CreateElement("ClaveVigencia");
            clavevigencia.Attributes.Append(urlVigencia);
            clavevigencia.InnerText = "0";
            XmlNode claveServicio = doc.CreateElement("ClaveServicio");
            claveServicio.Attributes.Append(urlServicio);
            claveServicio.InnerText = servicio;
            XmlNode claveDescuento = doc.CreateElement("ClaveDescuento");
            claveDescuento.Attributes.Append(urlDescuento);
            claveDescuento.InnerText = "0";
            XmlNode claveAgente = doc.CreateElement("ClaveAgente");
            claveAgente.Attributes.Append(urlAgente);
            claveAgente.InnerText = "9257";
            XmlNode claveFormaPago = doc.CreateElement("ClaveFormaPago");
            claveFormaPago.Attributes.Append(urlFormaPago);
            claveFormaPago.InnerText = "0";
            //fin nodo caracteristicasCotizacio

            //inicio nodo datosAutos
            XmlNode datosAuto = doc.CreateElement("datosAuto");
            datosAuto.Attributes.Append(urlDatosAuto);
            XmlNode ClaveProducto = doc.CreateElement("ClaveProducto");
            ClaveProducto.Attributes.Append(urlClaveProducto);
            ClaveProducto.InnerText = "1";
            XmlNode Tarifa = doc.CreateElement("Tarifa");
            Tarifa.Attributes.Append(urlTarifa);
            Tarifa.InnerText = "1704";
            XmlNode TipoVehiculo = doc.CreateElement("TipoVehiculo");
            TipoVehiculo.Attributes.Append(urlTipoVehiculo);
            TipoVehiculo.InnerText = "AU";
            XmlNode clavePerfil = doc.CreateElement("ClavePerfil");
            clavePerfil.Attributes.Append(urlClavePerfil);
            clavePerfil.InnerText = "36013";
            XmlNode claveModelo = doc.CreateElement("ClaveModelo");
            claveModelo.Attributes.Append(urlClaveModelo);
            claveModelo.InnerText = "2019";
            XmlNode claveMarca = doc.CreateElement("ClaveMarca");
            claveMarca.Attributes.Append(urlMarca);
            claveMarca.InnerText = "ACURA";
            XmlNode CLaveSubMarca = doc.CreateElement("ClaveSubMarca");
            CLaveSubMarca.Attributes.Append(urlSubMarca);
            CLaveSubMarca.InnerText = "RDX";
            XmlNode ClaveVehiculo = doc.CreateElement("ClaveVehiculo");
            ClaveVehiculo.Attributes.Append(urlClaveVehiculo);
            ClaveVehiculo.InnerText = "M00100011";
            XmlNode NumeroSerieAuto = doc.CreateElement("NumeroSerieAuto");//Vacio
            NumeroSerieAuto.Attributes.Append(urlNumSerieAuto);
            XmlNode SerieValida = doc.CreateElement("SerieValida");
            SerieValida.Attributes.Append(urlSerieValida);
            SerieValida.InnerText = "false";
            XmlNode NumeroPlacas = doc.CreateElement("NumeroPlacas");//Nodo vacio
            NumeroPlacas.Attributes.Append(urlNumPlacas);
            XmlNode NumeroMotor = doc.CreateElement("NumeroMotor");//Nodo vacio
            NumeroMotor.Attributes.Append(urlNumeroMotor);
            XmlNode NumeroControlVehicular = doc.CreateElement("NumeroControlVehicular");//Nodo Vacio
            NumeroControlVehicular.Attributes.Append(urlControlVehicular);
            XmlNode NombreConductor = doc.CreateElement("NombreConductor"); //nodo vacio
            NombreConductor.Attributes.Append(urlConductor);
            XmlNode BeneficiarioPreferente = doc.CreateElement("BeneficiarioPreferente");//nodo vacio
            BeneficiarioPreferente.Attributes.Append(urlBeneficiario);
            //------Fin nodo datosAuto

            //Nodos sin etiquetas
            XmlNode ValorConvenido = doc.CreateElement("ValorConvenido");
            ValorConvenido.Attributes.Append(urlValorCon);
            ValorConvenido.InnerText = "0";
            XmlNode FechaFactura = doc.CreateElement("FechaFactura");//Nodo vacio
            FechaFactura.Attributes.Append(urlFechaFact);
            XmlNode ValorFactura = doc.CreateElement("ValorFactura");
            ValorFactura.Attributes.Append(urlValorFactura);
            ValorFactura.InnerText = "0";

            //Nodos de coberturasAmparadas
            XmlNode coberturasAmparadas = doc.CreateElement("coberturasAmparadas");
            coberturasAmparadas.Attributes.Append(urlCoberturaAmpa);
            //Inicio CoberturaAmparada1
            XmlNode CoberturaAmparada1 = doc.CreateElement("CoberturaAmparada");
            XmlNode Amparada = doc.CreateElement("Amparada");
            Amparada.InnerText = "true";
            XmlNode Clave = doc.CreateElement("Clave");
            Clave.InnerText = "RT";
            XmlNode Descripcion = doc.CreateElement("Descripcion");
            Descripcion.InnerText = "ROBO TOTAL";
            XmlNode SumaAsegurada = doc.CreateElement("SumaAsegurada");
            SumaAsegurada.InnerText = "0";
            XmlNode DescripcionSumaAsegurada = doc.CreateElement("DescripcionSumaAsegurada");
            DescripcionSumaAsegurada.InnerText = "VALOR COMERCIAL";
            XmlNode PorcentajeDeducible = doc.CreateElement("PorcentajeDeducible");
            PorcentajeDeducible.InnerText = "5";
            XmlNode DescripcionDeducible = doc.CreateElement("DescripcionDeducible");
            DescripcionDeducible.InnerText = "5%";
            XmlNode PrimaNeta = doc.CreateElement("PrimaNeta");//NoNO NULL
            PrimaNeta.InnerText ="null";
            //Fin nodo coberturaAmparada1

            //Inicio CoberturaAmparada2
            XmlNode CoberturaAmparada2 = doc.CreateElement("CoberturaAmparada");
            XmlNode Amparada2 = doc.CreateElement("Amparada");
            Amparada2.InnerText = "true";
            XmlNode Clave2 = doc.CreateElement("Clave");
            Clave2.InnerText = "RC";
            XmlNode Descripcion2 = doc.CreateElement("Descripcion");
            Descripcion2.InnerText = "RESPONSBILIDAD CIVIL L.U.C.*";
            XmlNode SumaAsegurada2 = doc.CreateElement("SumaAsegurada");
            SumaAsegurada2.InnerText = "750000";
            XmlNode DescripcionSumaAsegurada2 = doc.CreateElement("DescripcionSumaAsegurada");
            DescripcionSumaAsegurada2.InnerText = "750,000.00";
            XmlNode PorcentajeDeducible2 = doc.CreateElement("PorcentajeDeducible");
            PorcentajeDeducible2.InnerText = "0";
            XmlNode DescripcionDeducible2 = doc.CreateElement("DescripcionDeducible");
            DescripcionDeducible2.InnerText = "0";
            XmlNode PrimaNeta2 = doc.CreateElement("PrimaNeta");//Nodo vacio
            PrimaNeta2.InnerText = "null";

            //Fin nodo coberturaAmparada2
            //Inicio CoberturaAmparada3
            XmlNode CoberturaAmparada3 = doc.CreateElement("CoberturaAmparada");
            XmlNode Amparada3 = doc.CreateElement("Amparada");
            Amparada3.InnerText = "true";
            XmlNode Clave3 = doc.CreateElement("Clave");
            Clave3.InnerText = "RCE";
            XmlNode Descripcion3 = doc.CreateElement("Descripcion");
            Descripcion3.InnerText = "EXTENSION DE RESPONSABILIDAD CIVIL";
            XmlNode SumaAsegurada3 = doc.CreateElement("SumaAsegurada");
            SumaAsegurada3.InnerText = "0";
            XmlNode DescripcionSumaAsegurada3 = doc.CreateElement("DescripcionSumaAsegurada");
            DescripcionSumaAsegurada3.InnerText = "AMPARADA";
            XmlNode PorcentajeDeducible3 = doc.CreateElement("PorcentajeDeducible");
            PorcentajeDeducible3.InnerText = "0";
            XmlNode DescripcionDeducible3 = doc.CreateElement("DescripcionDeducible");
            DescripcionDeducible3.InnerText = "0";
            XmlNode PrimaNeta3 = doc.CreateElement("PrimaNeta");//Nodo vacio
            PrimaNeta3.InnerText = "null";

            //Fin nodo coberturaAmparada3
            //Inicio CoberturaAmparada4
            XmlNode CoberturaAmparada4 = doc.CreateElement("CoberturaAmparada");
            XmlNode Amparada4 = doc.CreateElement("Amparada");
            Amparada4.InnerText = "true";
            XmlNode Clave4 = doc.CreateElement("Clave");
            Clave4.InnerText = "RCC";
            XmlNode Descripcion4 = doc.CreateElement("Descripcion");
            Descripcion4.InnerText = "EXCESO DE RC A PERSONAS ART.502";
            XmlNode SumaAsegurada4 = doc.CreateElement("SumaAsegurada");
            SumaAsegurada4.InnerText = "500000";
            XmlNode DescripcionSumaAsegurada4 = doc.CreateElement("DescripcionSumaAsegurada");
            DescripcionSumaAsegurada4.InnerText = "500,000.00";
            XmlNode PorcentajeDeducible4 = doc.CreateElement("PorcentajeDeducible");
            PorcentajeDeducible4.InnerText = "0";
            XmlNode DescripcionDeducible4 = doc.CreateElement("DescripcionDeducible");
            DescripcionDeducible4.InnerText = "0";
            XmlNode PrimaNeta4 = doc.CreateElement("PrimaNeta");//Nodo vacio
            PrimaNeta4.InnerText = "null";

            //Fin nodo coberturaAmparada4
            //Inicio CoberturaAmparada5
            XmlNode CoberturaAmparada5 = doc.CreateElement("CoberturaAmparada");
            XmlNode Amparada5 = doc.CreateElement("Amparada");
            Amparada5.InnerText = "true";
            XmlNode Clave5 = doc.CreateElement("Clave");
            Clave5.InnerText = "GM";
            XmlNode Descripcion5 = doc.CreateElement("Descripcion");
            Descripcion5.InnerText = "GASTOS MEDICOS OCUPANTES L.U.C.*";
            XmlNode SumaAsegurada5 = doc.CreateElement("SumaAsegurada");
            SumaAsegurada5.InnerText = "125000";
            XmlNode DescripcionSumaAsegurada5 = doc.CreateElement("DescripcionSumaAsegurada");
            DescripcionSumaAsegurada5.InnerText = "125,000.00";
            XmlNode PorcentajeDeducible5 = doc.CreateElement("PorcentajeDeducible");
            PorcentajeDeducible5.InnerText = "0";
            XmlNode DescripcionDeducible5 = doc.CreateElement("DescripcionDeducible");
            DescripcionDeducible5.InnerText = "0";
            XmlNode PrimaNeta5 = doc.CreateElement("PrimaNeta");//Nodo vacio
            PrimaNeta5.InnerText = "null";

            //Fin nodo coberturaAmparada5
            //Inicio CoberturaAmparada6
            XmlNode CoberturaAmparada6 = doc.CreateElement("CoberturaAmparada");
            XmlNode Amparada6 = doc.CreateElement("Amparada");
            Amparada6.InnerText = "true";
            XmlNode Clave6 = doc.CreateElement("Clave");
            Clave6.InnerText = "DJ";
            XmlNode Descripcion6 = doc.CreateElement("Descripcion");
            Descripcion6.InnerText = "ASESORIA LEGAL, FIANZAS Y/O CAUCIONES";
            XmlNode SumaAsegurada6 = doc.CreateElement("SumaAsegurada");
            SumaAsegurada6.InnerText = "0";
            XmlNode DescripcionSumaAsegurada6 = doc.CreateElement("DescripcionSumaAsegurada");
            DescripcionSumaAsegurada6.InnerText = "AMPARADA";
            XmlNode PorcentajeDeducible6 = doc.CreateElement("PorcentajeDeducible");
            PorcentajeDeducible6.InnerText = "0";
            XmlNode DescripcionDeducible6 = doc.CreateElement("DescripcionDeducible");
            DescripcionDeducible6.InnerText = "0";
            XmlNode PrimaNeta6 = doc.CreateElement("PrimaNeta");//Nodo vacio
            PrimaNeta6.InnerText = "null";

            //Fin nodo coberturaAmparada6
            //Inicio CoberturaAmparada7
            XmlNode CoberturaAmparada7 = doc.CreateElement("CoberturaAmparada");
            XmlNode Amparada7 = doc.CreateElement("Amparada");
            Amparada7.InnerText = "true";
            XmlNode Clave7 = doc.CreateElement("Clave");
            Clave7.InnerText = "AI";
            XmlNode Descripcion7 = doc.CreateElement("Descripcion");
            Descripcion7.InnerText = "ASISTENCIA VIAL";
            XmlNode SumaAsegurada7 = doc.CreateElement("SumaAsegurada");
            SumaAsegurada7.InnerText = "0";
            XmlNode DescripcionSumaAsegurada7 = doc.CreateElement("DescripcionSumaAsegurada");
            DescripcionSumaAsegurada7.InnerText = "AMPARADA";
            XmlNode PorcentajeDeducible7 = doc.CreateElement("PorcentajeDeducible");
            PorcentajeDeducible7.InnerText = "0";
            XmlNode DescripcionDeducible7 = doc.CreateElement("DescripcionDeducible");
            DescripcionDeducible7.InnerText = "0";
            XmlNode PrimaNeta7 = doc.CreateElement("PrimaNeta");//Nodo vacio
            PrimaNeta7.InnerText = "null";

            //Fin nodo coberturaAmparada7
            //Inicio CoberturaAmparada8
            XmlNode CoberturaAmparada8 = doc.CreateElement("CoberturaAmparada");
            XmlNode Amparada8 = doc.CreateElement("Amparada");
            Amparada8.InnerText = "true";
            XmlNode Clave8 = doc.CreateElement("Clave");
            Clave8.InnerText = "MA";
            XmlNode Descripcion8 = doc.CreateElement("Descripcion");
            Descripcion8.InnerText = "ACCIDENTES AL CONDUCTOR";
            XmlNode SumaAsegurada8 = doc.CreateElement("SumaAsegurada");
            SumaAsegurada8.InnerText = "100000";
            XmlNode DescripcionSumaAsegurada8 = doc.CreateElement("DescripcionSumaAsegurada");
            DescripcionSumaAsegurada8.InnerText = "100,000.00";
            XmlNode PorcentajeDeducible8 = doc.CreateElement("PorcentajeDeducible");
            PorcentajeDeducible8.InnerText = "0";
            XmlNode DescripcionDeducible8 = doc.CreateElement("DescripcionDeducible");
            DescripcionDeducible8.InnerText = "0";
            XmlNode PrimaNeta8 = doc.CreateElement("PrimaNeta");//Nodo vacio
            PrimaNeta8.InnerText = "null";

            //Fin nodo coberturaAmparada6
            //FIn del grupo de nodos de datosautos

            //Estructura de grupo de nodos amparadas 
            coberturasAmparadas.AppendChild(CoberturaAmparada1);
            coberturasAmparadas.AppendChild(CoberturaAmparada2);
            coberturasAmparadas.AppendChild(CoberturaAmparada3);
            coberturasAmparadas.AppendChild(CoberturaAmparada4);
            coberturasAmparadas.AppendChild(CoberturaAmparada5);
            coberturasAmparadas.AppendChild(CoberturaAmparada6);
            coberturasAmparadas.AppendChild(CoberturaAmparada7);
            coberturasAmparadas.AppendChild(CoberturaAmparada8);

            CoberturaAmparada1.AppendChild(Amparada);
            CoberturaAmparada1.AppendChild(Clave);
            CoberturaAmparada1.AppendChild(Descripcion);
            CoberturaAmparada1.AppendChild(SumaAsegurada);
            CoberturaAmparada1.AppendChild(DescripcionSumaAsegurada);
            CoberturaAmparada1.AppendChild(PorcentajeDeducible);
            CoberturaAmparada1.AppendChild(DescripcionDeducible);
            CoberturaAmparada1.AppendChild(PrimaNeta);

            CoberturaAmparada2.AppendChild(Amparada2);
            CoberturaAmparada2.AppendChild(Clave2);
            CoberturaAmparada2.AppendChild(Descripcion2);
            CoberturaAmparada2.AppendChild(SumaAsegurada2);
            CoberturaAmparada2.AppendChild(DescripcionSumaAsegurada2);
            CoberturaAmparada2.AppendChild(PorcentajeDeducible2);
            CoberturaAmparada2.AppendChild(DescripcionDeducible2);
            CoberturaAmparada2.AppendChild(PrimaNeta2);

            CoberturaAmparada3.AppendChild(Amparada3);
            CoberturaAmparada3.AppendChild(Clave3);
            CoberturaAmparada3.AppendChild(Descripcion3);
            CoberturaAmparada3.AppendChild(SumaAsegurada3);
            CoberturaAmparada3.AppendChild(DescripcionSumaAsegurada3);
            CoberturaAmparada3.AppendChild(PorcentajeDeducible3);
            CoberturaAmparada3.AppendChild(DescripcionDeducible3);
            CoberturaAmparada3.AppendChild(PrimaNeta3);

            CoberturaAmparada4.AppendChild(Amparada4);
            CoberturaAmparada4.AppendChild(Clave4);
            CoberturaAmparada4.AppendChild(Descripcion4);
            CoberturaAmparada4.AppendChild(SumaAsegurada4);
            CoberturaAmparada4.AppendChild(DescripcionSumaAsegurada4);
            CoberturaAmparada4.AppendChild(PorcentajeDeducible4);
            CoberturaAmparada4.AppendChild(DescripcionDeducible4);
            CoberturaAmparada4.AppendChild(PrimaNeta4);

            CoberturaAmparada5.AppendChild(Amparada5);
            CoberturaAmparada5.AppendChild(Clave5);
            CoberturaAmparada5.AppendChild(Descripcion5);
            CoberturaAmparada5.AppendChild(SumaAsegurada5);
            CoberturaAmparada5.AppendChild(DescripcionSumaAsegurada5);
            CoberturaAmparada5.AppendChild(PorcentajeDeducible5);
            CoberturaAmparada5.AppendChild(DescripcionDeducible5);
            CoberturaAmparada5.AppendChild(PrimaNeta5);

            CoberturaAmparada6.AppendChild(Amparada6);
            CoberturaAmparada6.AppendChild(Clave6);
            CoberturaAmparada6.AppendChild(Descripcion6);
            CoberturaAmparada6.AppendChild(SumaAsegurada6);
            CoberturaAmparada6.AppendChild(DescripcionSumaAsegurada6);
            CoberturaAmparada6.AppendChild(PorcentajeDeducible6);
            CoberturaAmparada6.AppendChild(DescripcionDeducible6);
            CoberturaAmparada6.AppendChild(PrimaNeta6);

            CoberturaAmparada7.AppendChild(Amparada7);
            CoberturaAmparada7.AppendChild(Clave7);
            CoberturaAmparada7.AppendChild(Descripcion7);
            CoberturaAmparada7.AppendChild(SumaAsegurada7);
            CoberturaAmparada7.AppendChild(DescripcionSumaAsegurada7);
            CoberturaAmparada7.AppendChild(PorcentajeDeducible7);
            CoberturaAmparada7.AppendChild(DescripcionDeducible7);
            CoberturaAmparada7.AppendChild(PrimaNeta7);

            CoberturaAmparada8.AppendChild(Amparada8);
            CoberturaAmparada8.AppendChild(Clave8);
            CoberturaAmparada8.AppendChild(Descripcion8);
            CoberturaAmparada8.AppendChild(SumaAsegurada8);
            CoberturaAmparada8.AppendChild(DescripcionSumaAsegurada8);
            CoberturaAmparada8.AppendChild(PorcentajeDeducible8);
            CoberturaAmparada8.AppendChild(DescripcionDeducible8);
            CoberturaAmparada8.AppendChild(PrimaNeta8);
            //Fin estructira de coberturas

            //Estructura valor convenio, fecha factura, valor factura


            //valor convenido
            //Datos autos
            datosAuto.AppendChild(ClaveProducto);
            datosAuto.AppendChild(Tarifa);
            datosAuto.AppendChild(TipoVehiculo);
            datosAuto.AppendChild(clavePerfil);
            datosAuto.AppendChild(claveModelo);
            datosAuto.AppendChild(claveMarca);
            datosAuto.AppendChild(CLaveSubMarca);
            datosAuto.AppendChild(ClaveVehiculo);
            datosAuto.AppendChild(NumeroSerieAuto);
            datosAuto.AppendChild(SerieValida);
            datosAuto.AppendChild(NumeroPlacas);
            datosAuto.AppendChild(NumeroMotor);
            datosAuto.AppendChild(NumeroControlVehicular);
            datosAuto.AppendChild(NombreConductor);
            datosAuto.AppendChild(BeneficiarioPreferente);
            //Fin de estructura datos auto
            //Estructura carateristicas cotizacion
            caracteristicasCotizacion.AppendChild(claveEstado);
            caracteristicasCotizacion.AppendChild(clavePaquete);
            caracteristicasCotizacion.AppendChild(clavevigencia);
            caracteristicasCotizacion.AppendChild(claveServicio);
            caracteristicasCotizacion.AppendChild(claveDescuento);
            caracteristicasCotizacion.AppendChild(claveAgente);
            caracteristicasCotizacion.AppendChild(claveFormaPago);
            //fin Estructura carateristicas cotizacion

            credenciales.AppendChild(IdApp);
            credenciales.AppendChild(PassApp);
            credenciales.AppendChild(claveUsuario);
            credenciales.AppendChild(Password);

            datosCotizar.AppendChild(credenciales);
            datosCotizar.AppendChild(caracteristicasCotizacion);
            datosCotizar.AppendChild(datosAuto);
            datosCotizar.AppendChild(ValorConvenido);
            datosCotizar.AppendChild(FechaFactura);
            datosCotizar.AppendChild(ValorFactura);
            datosCotizar.AppendChild(coberturasAmparadas);

            rootNode.AppendChild(datosCotizar);
            bodyNode.AppendChild(rootNode);
            evelope.AppendChild(bodyNode);

            doc.Save("C:\\Users\\Eduardo Medina\\Desktop\\Xml3_0.xml");
            xml = doc.OuterXml;
            return xml;
        }


    }
}