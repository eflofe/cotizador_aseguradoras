﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CotizadorAseguradoras
{
    public class LogCotizacionHogar
    {
        MySqlConnection con = new MySqlConnection();
        string servidor = "Server=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;";
        string puerto = "3306";
        string usuario = "Uid=servici1;";
        string pass = "pwd=UUpzNvi9huoAF4SX;";
        string dataBase = "Database=ws_db;";

        public string InsertLog(string JsonRequest)
        {
            try
            {
                string cadenaCon = servidor + dataBase + usuario + pass;

                MySqlDataAdapter adapter = new MySqlDataAdapter();
                con.ConnectionString = cadenaCon;
                con.Open();
                var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string query = "INSERT INTO LogCotHogar (JSONRequest,RequestWS,ResponseWS,JSONResponse,Aseguradora,FechaInicio,FechaFin)" +
                    "VALUES('" + JsonRequest + "',NULL,NULL,NULL,'AbaTEST', '" + fechaInicio + "',NULL)";
                MySqlCommand command = new MySqlCommand(query, con);
                command.ExecuteNonQuery();
                con.Close();
                var Id = GetLastId();
                return Id;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return error;
            }
        }
        public void UpdateRequest(string xml, string id)
        {
            try
            {
                string cadenaCon = servidor + dataBase + usuario + pass;

                MySqlDataAdapter adapter = new MySqlDataAdapter();
                con.ConnectionString = cadenaCon;
                con.Open();
                var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string query = "Update LogCotHogar set RequestWS = '" + xml + "' where IdLogCot = " + id;
                MySqlCommand command = new MySqlCommand(query, con);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
            }
        }

        public void UpdateResponse(string xml, string id,string JsonResponse)
        {
            try
            {
                string cadenaCon = servidor + dataBase + usuario + pass;

                MySqlDataAdapter adapter = new MySqlDataAdapter();
                con.ConnectionString = cadenaCon;
                con.Open();
                var fechaFin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string query = "Update LogCotHogar set ResponseWS = '" + xml + "', JsonResponse = '" + JsonResponse + "', FechaFin='" + fechaFin + "' where IdLogCot = " + id;
                MySqlCommand command = new MySqlCommand(query, con);
                command.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
            }
        }

        public string GetLastId()
        {
            try
            {
                string cadenaCon = servidor + dataBase + usuario + pass;
                con.Open();
                string query = "select MAX(IdLogCot) from LogCotHogar";
                MySqlCommand command = new MySqlCommand(query, con);
                command.ExecuteNonQuery();
                string Id = command.ExecuteScalar().ToString();
                con.Close();
                return Id;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return error;
            }
        }
    }
}