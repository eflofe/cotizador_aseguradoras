﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CotizadorAseguradoras
{
    public class FormatoJsonGenericoBQ
    {
        Vehiculo vr = new Vehiculo();
        Direccion dR = new Direccion();
        LogCotizacion logC = new LogCotizacion();
        DatosJson datosJs = new DatosJson();

        public string JsonGenericoQB(string descripcion, string aseguradora, string clave, string codPostal, string descuento, string edad, string fechaNacimiento, string genero, string marca, int modelo, string paquete, string servicio)
        {
            //Vehiculo
            vr.Marca = marca;
            vr.Modelo = modelo.ToString();
            //vr.SubMarca = descripcionInicial;
            vr.Servicio = servicio;
            vr.Uso = "PARTICULAR";
            vr.Modelo = modelo.ToString();
            vr.Clave = clave;
            string clave_descripcion = vr.Clave;
            string clave_Marca = vr.Marca;
            string año = vr.Modelo;
             vr.Descripcion = descripcion;
            //cliente
            Cliente cl = new Cliente();
            cl.FechaNacimiento = fechaNacimiento;
            cl.Genero = genero;
            dR.CodPostal = codPostal;
            cl.direccion = dR;
            cl.Edad = edad;
            cl.FechaNacimiento = fechaNacimiento;
            cl.Telefono = "5544778899";
            //Datos afuera 
            datosJs.Descuento = descuento;
            datosJs.paquete = paquete;
            datosJs.Aseguradora = aseguradora;
            datosJs.PeriodicidadDePago = "0";
            //Cotizacion
            Cotizacion cT = new Cotizacion();
            Emision eM = new Emision();
            Pago pG = new Pago();
            pG.Carrier = "0";
            //LLenado de datos
            datosJs.cliente = cl;
            datosJs.vehiculo = vr;
            datosJs.cotizacion = cT;
            datosJs.emision = eM;
            datosJs.pago = pG;
            string response = JsonConvert.SerializeObject(datosJs);
            return response;
        }

        /// <summary>
        /// Métodos get and setter para el ambiente de pruebas
        /// </summary>
        public partial class Marcas
        {

            public string Clave_marca { get; set; }
        }

        public partial class Modelos
        {
            public long modelo { get; set; }
        }
        public partial class DescripcionAuto
        {
            public string Descripcion { get; set; }
        }

        public partial class Clave
        {
            public string Clave_descripcion { get; set; }
        }

        /// <summary>
        /// Método donde se almacenan los datos de el coche
        /// </summary>
        public partial class Aseguradoras
        {
            [JsonProperty("Aseguradora")]
            public string Aseguradora { get; set; }
        }

        public partial class DatosJson
        {
            public Cliente cliente { get; set; }
            public Cotizacion cotizacion { get; set; }
            public Emision emision { get; set; }
            public string Descuento { get; set; }
            public Vehiculo vehiculo { get; set; }
            public Pago pago { get; set; }
            public string paquete { get; set; }
            public string Aseguradora { get; set; }
            public string PeriodicidadDePago { get; set; }

        }

        public partial class Cliente
        {
            public string FechaNacimiento { get; set; }
            public string Genero { get; set; }
            public Direccion direccion { get; set; }
            public string Telefono { get; set; }
            public string Edad { get; set; }

        }

        public partial class Direccion
        {
            public string CodPostal { get; set; }

        }

        public partial class Cotizacion
        {

        }
        public partial class Emision
        {

        }
        public partial class Vehiculo
        {
            public string Descripcion { get; set; }
            public string Uso { get; set; }
            public string Marca { get; set; }
            public string Clave { get; set; }
            public string Servicio { get; set; }
            //public string SubMarca { get; set; }
            public string Modelo { get; set; }

        }

        public partial class Pago
        {
            public string Carrier { get; set; }
        }

    }
}