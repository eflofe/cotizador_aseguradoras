﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CotizadorAseguradoras
{
    public class LogCotizacion
    {
        private string iD = "";
        private string estado = "";
        private string descripcion = "";
        private string subMarcaLatino = "";
        private string claveDescripcion = "";

        SqlConnection con = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Replica_Ali_R;user id = servici1; password = Master@011");
        string connectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db;";
        private string codigo_postalEstadoMexico = "";
        private string codigo_postalEstadoMexicoDosNumeros = "";
        private string codigo_postalExterno = "";
        private string codigo_postalEcatepec = "";
        private string codigo_postalChiapas = "";
        private string codigo_postalCiudadMexico = "";
        private string codigo_postalJalisco = "";
        private string codigo_postalMichoacan = "";
        private string codigo_postalNuevoLeon = "";
        private string codigo_postalSinaloa = "";
        private string codigo_postalSonora = "";
        private string codigo_postalTamaulipas = "";
        private string codigo_postalIxtapaluca = "";
        private string codigo_postaBajaCalifornia = "";
        private string codigo_postaQuintanaRoo = "";
        private string codigo_postaHidalgo = "";
        private string codigo_postalVeracruz = "";
        private string codigo_postalGuerrero = "";


        public string InsertLog(string JSONrequest, string Aseguradora, string Marca, string Modelo, string Descripcion, string clave)
        {
            try
            {
                var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string query = "Insert into ReplicaDeAli_LogWSCotizacion (JSONRequest,RequestWS,ResponseWS,JSONResponse,Aseguradora,Marca,Modelo,Descripcion,clave,FechaInicio,FechaFin)" +
        "VALUES ('" + JSONrequest + "',NULL,NULL,NULL,'" + Aseguradora + "','" + Marca + "','" + Modelo + "','" + Descripcion + "','" + clave + "'," +
        "'" + fechaInicio + "',NULL)";
                MySqlConnection miConexion = new MySqlConnection(connectionString);
                MySqlCommand miComando = new MySqlCommand(query, miConexion);
                MySqlDataReader leerDatos;
                miConexion.Open();
                leerDatos = miComando.ExecuteReader();
                miConexion.Close();
                var IdReturned = GetLastId();
                return IdReturned;
            }
            catch (Exception ex)
            {
                throw new ArgumentException("error al insertar el log " + ex.Message);
            }
        }

        public string GetLastId()
        {
            try
            {
                MySqlDataReader MyReader2;
                MySqlConnection connection = new MySqlConnection(connectionString);
                string query = "select MAX(idLogWSCot)  from `ws_db`.`ReplicaDeAli_LogWSCotizacion`";
                MySqlCommand myCommand = new MySqlCommand(query, connection);
                connection.Open();
                MyReader2 = myCommand.ExecuteReader();
                while (MyReader2.Read())
                {
                    iD = MyReader2.GetValue(0).ToString();
                }
                MyReader2.Close();
                myCommand.Dispose();
                connection.Close();
                return iD;
            }
            catch (Exception e)
            {
                return "Error" + e;
            }
        }
        public bool UpdateLogRequest(string XmlRequest, string id)
        {
            try
            {
                string queryUpdate = "Update ReplicaDeAli_LogWSCotizacion set RequestWS ='" + XmlRequest + "' WHERE idLogWSCot =" + id;
                MySqlConnection miConexion = new MySqlConnection(connectionString);
                MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
                MySqlDataReader leerDatos;
                miConexion.Open();
                leerDatos = miComando.ExecuteReader();
                miConexion.Close();
                return true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
            }
        }

        public bool UpdateLogResponse(string XmlResponse, string id)
        {
            try
            {
                string queryUpdate = "Update ReplicaDeAli_LogWSCotizacion set ResponseWS ='" + XmlResponse + "' WHERE idLogWSCot =" + id;
                MySqlConnection miConexion = new MySqlConnection(connectionString);
                MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
                MySqlDataReader leerDatos;
                miConexion.Open();
                leerDatos = miComando.ExecuteReader();
                miConexion.Close();
                return true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade XML response" + e.Message);
            }
        }

        public bool UpdateLogJsonResponse(string JsonResponse, string id)
        {
            try
            {
                var fechaFin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string queryUpdate = "Update ReplicaDeAli_LogWSCotizacion set JSONResponse ='" + JsonResponse + "', FechaFin='" + fechaFin + "' WHERE idLogWSCot =" + GetLastId();
                MySqlConnection miConexion = new MySqlConnection(connectionString);
                MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
                MySqlDataReader leerDatos;
                miConexion.Open();
                leerDatos = miComando.ExecuteReader();
                miConexion.Close();
                return true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade JSON request" + e.Message);
            }
        }

        //Datos a guardar de Emisión

        /// <summary>
        /// Método que guarda la peticion Json de emisión
        /// </summary>
        /// <param name="JSONrequest"></param>
        /// <param name="Aseguradora"></param>
        /// <param name="Marca"></param>
        /// <param name="Modelo"></param>
        /// <param name="Descripcion"></param>
        /// <param name="clave"></param>
        /// <returns>Ultimo id afectado</returns>
        public string InsertLogEmision(string JSONrequest, string Aseguradora, string Marca, string Modelo, string Descripcion, string clave)
        {
            try
            {
                var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string query = "Insert into ReplicaDeAli_LogWSEmision (JSONRequest,ResponseWS,JSONResponse,Aseguradora,Marca,Modelo,Descripcion,clave,FechaInicio,FechaFin)" +
        "VALUES ('" + JSONrequest + "',NULL,NULL,'" + Aseguradora + "','" + Marca + "','" + Modelo + "','" + Descripcion + "','" + clave + "'," +
        "'" + fechaInicio + "',NULL)";
                MySqlConnection miConexion = new MySqlConnection(connectionString);
                MySqlCommand miComando = new MySqlCommand(query, miConexion);
                MySqlDataReader leerDatos;
                miConexion.Open();
                leerDatos = miComando.ExecuteReader();
                miConexion.Close();
                var IdReturned = GetLastIdEmision();
                return IdReturned;
            }
            catch (Exception ex)
            {
                throw new ArgumentException("error al insertar el log " + ex.Message);
            }
        }

        public bool UpdateLogResponseEmision(string XmlResponse, string id)
        {
            try
            {
                string queryUpdate = "Update ReplicaDeAli_LogWSEmision set ResponseWS ='" + XmlResponse + "' WHERE idLogWSCot =" + id;
                MySqlConnection miConexion = new MySqlConnection(connectionString);
                MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
                MySqlDataReader leerDatos;
                miConexion.Open();
                leerDatos = miComando.ExecuteReader();
                miConexion.Close();
                return true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade XML response" + e.Message);
            }
        }


        public bool UpdateLogJsonResponseEmision(string JsonResponse, string id)
        {
            try
            {
                var fechaFin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string queryUpdate = "Update ReplicaDeAli_LogWSEmision set JSONResponse ='" + JsonResponse + "', FechaFin='" + fechaFin + "' WHERE idLogWSCot =" + id;
                MySqlConnection miConexion = new MySqlConnection(connectionString);
                MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
                MySqlDataReader leerDatos;
                miConexion.Open();
                leerDatos = miComando.ExecuteReader();
                miConexion.Close();
                return true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade JSON request" + e.Message);
            }
        }



        public string GetLastIdEmision()
        {
            try
            {
                MySqlDataReader MyReader2;
                MySqlConnection connection = new MySqlConnection(connectionString);
                string query = "select MAX(idLogWSCot)  from `ws_db`.`ReplicaDeAli_LogWSEmision`";
                MySqlCommand myCommand = new MySqlCommand(query, connection);
                connection.Open();
                MyReader2 = myCommand.ExecuteReader();
                while (MyReader2.Read())
                {
                    iD = MyReader2.GetValue(0).ToString();
                }
                MyReader2.Close();
                myCommand.Dispose();
                connection.Close();
                return iD;
            }
            catch (Exception e)
            {
                return "Error" + e;
            }
        }



        //Extraer datos por aseguradora

        /// <summary>
        /// Método que obtiene los las marcas de la aseguradora ingresada
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <param name="aseguradora"></param>
        /// <returns></returns>
        public string GetMarcaAseguradora(string aseguradora)
        {
            try
            {
                string queryEstado = "Select Distinct IDEdo_Qua from CatEdosQUA where CPostal ='" + aseguradora + "'";
                SqlDataAdapter da = new SqlDataAdapter(queryEstado, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }

            catch (Exception ex)
            {
                return "Error: " + ex;
            }

        }

        /// <summary>
        /// Método que obtiene los modelos de las aseguradoras
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <param name="marca"></param>
        /// <returns>Json Marcas</returns>
        public string GetModelosAseguradora(string aseguradora, string marca)
        {
            try
            {
                string queryEstado = "Select Distinct IDEdo_Qua from CatEdosQUA where CPostal ='" + aseguradora + "'";
                SqlDataAdapter da = new SqlDataAdapter(queryEstado, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }

            catch (Exception ex)
            {
                return "Error: " + ex;
            }

        }

        /// <summary>
        /// Método que recupera las descipciones por aseguradora
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <returns></returns>
        public string GetDescripcionesAseguradora(string aseguradora, string marca, string modelo)
        {
            try
            {
                string queryEstado = "Select Distinct IDEdo_Qua from CatEdosQUA where CPostal ='" + aseguradora + "'";
                SqlDataAdapter da = new SqlDataAdapter(queryEstado, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }
            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }

        /// <summary>
        /// Método que recupera los detalles por aseguradora ingresada
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <returns></returns>
        public string GetDetallesAseguradora(string aseguradora, string marca, string modelo, string descripcion, string subdescripcion)
        {
            try
            {
                string queryEstado = "Select Distinct IDEdo_Qua from CatEdosQUA where CPostal ='" + aseguradora + "'";
                SqlDataAdapter da = new SqlDataAdapter(queryEstado, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }
            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }

        /// <summary>
        /// Busca datos en especifico
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <param name="descripcion"></param>
        /// <param name="subdescripcion"></param>
        /// <returns></returns>
        public string Buscar(string aseguradora, string marca, string modelo, string descripcion, string subdescripcion, string detalle)
        {
            try
            {
                string queryEstado = "Select Distinct IDEdo_Qua from CatEdosQUA where CPostal ='" + aseguradora + "'";
                SqlDataAdapter da = new SqlDataAdapter(queryEstado, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }
            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }

        /// <summary>
        /// Método que busca el motor 
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <param name="descripcion"></param>
        /// <param name="subdescripcion"></param>
        /// <param name="detalle"></param>
        /// <returns></returns>
        public string BuscarMoto(string aseguradora, string marca, string modelo, string descripcion, string subdescripcion, string detalle)
        {
            try
            {
                string queryEstado = "Select Distinct IDEdo_Qua from CatEdosQUA where CPostal ='" + aseguradora + "'";
                SqlDataAdapter da = new SqlDataAdapter(queryEstado, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }
            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }

        //-.Query Qualitas 
        public string getEstado(string Cp)
        {
            string conn = (@"Data Source=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;Initial Catalog=ws_db;user id = servici1; password = UUpzNvi9huoAF4SX ");
            try
            {
                MySqlDataReader MyReader2;
                string queryEstado = "Select Distinct IDEdo_Qua from Ali_R_CatEdosQUA where CPostal ='" + Cp + "'";
                MySqlConnection conexion = new MySqlConnection(conn);
                MySqlCommand myCommand = new MySqlCommand(queryEstado, conexion);
                conexion.Open();
                MyReader2 = myCommand.ExecuteReader();
                while (MyReader2.Read())
                {
                    estado = MyReader2.GetValue(0).ToString();
                }
                MyReader2.Close();
                myCommand.Dispose();
                conexion.Close();
                return estado;
            }

            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }

        //---------Latino Query sql Server
        /// <summary>
        /// Metodo que recupera la clave de estado
        /// </summary>
        /// <param name="Cp"></param>
        /// <returns></returns>
        public string GetEstadoLatino(string Cp)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Replica_Ali_R;user id = servici1; password = Master@011");
            try
            {
                string queryEstado = "Select Distinct IDEdo_Qua from CatEdosQUA where CPostal ='" + Cp + "'";
                SqlCommand sqlEstado = new SqlCommand(queryEstado);
                sqlEstado.Connection = conn;
                conn.Open();
                sqlEstado.CommandTimeout = 0;
                sqlEstado.ExecuteNonQuery();
                string idEstado = sqlEstado.ExecuteScalar().ToString();
                conn.Close();
                return idEstado;
            }

            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }

        /// <summary>
        /// Metodo que obtiene las aseguradoras
        /// </summary>
        /// <returns></returns>
        public string GetAseguradoras()
        {
            try
            {
                SqlConnection cone = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Homologacion;user id = servici1; password = Master@011");
                string query = "exec Homologacion.dbo.Sp_GetAseguradoras";
                SqlDataAdapter da = new SqlDataAdapter(query, cone);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;
            }
            catch (Exception e)
            {
                return "" + e;
            }
        }

        public string GetModelosLati(string Marca, string SubMarca)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");
            try
            {
                string query = "exec LatinoCatalogos.dbo.Sp_GetModelos '" + Marca + "','" + SubMarca + "'";
                SqlDataAdapter da = new SqlDataAdapter(query, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;
            }
            catch (Exception e)
            {
                return "ERROR" + e;
            }

        }


        public string GetMarcasLati()
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");

            try
            {
                string query = "exec LatinoCatalogos.dbo.Sp_GetMarcas";
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }



        /// <summary>
        /// Metodo que obtien la submarca con un procedimiento
        /// </summary>
        /// <param name="Marca"></param>
        /// <returns></returns>
        public string GetSubMarcas(string Marca)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");

            try
            {
                string query = "exec Sp_GetSubMarcas'" + Marca + "'";
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;
            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        /// <summary>
        /// Trae descripciones de  modelo seleccionado
        /// </summary>
        /// <param name="Marca"></param>
        /// <param name="SubMarca"></param>
        /// <returns></returns>
        public string GetDescripciones(string Marca, string SubMarca, string modelo)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");
            try
            {
                string query = " exec LatinoCatalogos.dbo.Sp_GetDescripciones'" + Marca + "','" + SubMarca + "','" + modelo + "'";
                SqlDataAdapter da = new SqlDataAdapter(query, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;
            }
            catch (Exception e)
            {
                return "ERROR" + e;
            }

        }

        public string GetClabeDescripcion(string marca, string subMarca, string año, string descripcion)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");

            try
            {
                string query = "select LATINO_DESCRIPCIONES.CLAVE_DESCRIPCION from LATINO_DESCRIPCIONES where CLAVE_MARCA = '" + marca + "' and CLAVE_SUBMARCA =  '" + subMarca + "'and AÑO = '" + año + "' and descripcion = '" + descripcion + "';";
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;
            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }



        //Consultas MYSQL LATINO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="marca"></param>
        /// <param name="subMarca"></param>
        /// <param name="año"></param>
        /// <param name="descripcion"></param>
        /// <returns></returns>
        public string GetClabeDescripcionMysqlLatino(string marca, string subMarca, string año, string descripcion)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");

            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "SELECT CLAVE_DESCRIPCION FROM `ws_db`.`Latino_Catalogos_LATINO_DESCRIPCIONES` WHERE CLAVE_MARCA = '" + marca + "' and CLAVE_SUBMARCA = '" + subMarca + "' and AÑO = '" + año + "' and DESCRIPCION = '" + descripcion + "' ";
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                connection.Close();
                return json;
            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetMarcasLatiMysql()
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");

            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "SELECT DISTINCT(CLAVE_MARCA) FROM `ws_db`.`Latino_Catalogos_LATINO_DESCRIPCIONES` order by CLAVE_MARCA";
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                connection.Close();
                return json;
            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Marca"></param>
        /// <param name="SubMarca"></param>
        /// <returns></returns>
        public string GetModelosLatiMysql(string Marca, string SubMarca)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "SELECT DISTINCT(AÑO) FROM `ws_db`.`Latino_Catalogos_LATINO_DESCRIPCIONES` WHERE CLAVE_MARCA = '" + Marca + "' and CLAVE_SUBMARCA = '" + SubMarca + "' order by AÑO";
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                connection.Close();
                return json;
            }
            catch (Exception e)
            {
                return "ERROR" + e;
            }

        }

        /// <summary>
        /// Trae descripciones de  modelo seleccionado
        /// </summary>
        /// <param name="Marca"></param>
        /// <param name="SubMarca"></param>
        /// <returns></returns>
        public string GetDescripcionesMysqlatino(string Marca, string SubMarca, string modelo)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "SELECT DESCRIPCION FROM `ws_db`.`Latino_Catalogos_LATINO_DESCRIPCIONES` WHERE CLAVE_MARCA = '" + Marca + "' and CLAVE_SUBMARCA = '" + SubMarca + "' and AÑO = '" + modelo + "' ";
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                connection.Close();
                return json;
            }
            catch (Exception e)
            {
                return "ERROR" + e;
            }

        }

        /// <summary>
        /// Retorna la descripcion de el auto seleccionado
        /// </summary>
        /// <param name="marca"></param>
        /// <param name="subMarca"></param>
        /// <param name="año"></param>
        /// <returns></returns>
        public string GetDescripcion(string marca, string subMarca, string año, string claveD)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");

            try
            {
                MySqlDataReader MyReader2;
                MySqlConnection connection = new MySqlConnection(connectionString);
                string query = "select DESCRIPCION from `ws_db`.`Latino_Catalogos_LATINO_DESCRIPCIONES` where CLAVE_MARCA = '" + marca + "' and CLAVE_SUBMARCA =  '" + subMarca + "'and AÑO = '" + año + "' and DESCRIPCION = '" + claveD + "';";
                MySqlCommand myCommand = new MySqlCommand(query, connection);
                connection.Open();
                myCommand.CommandTimeout = 0;
                MyReader2 = myCommand.ExecuteReader();
                while (MyReader2.Read())
                {
                    descripcion = MyReader2.GetValue(0).ToString();
                }
                MyReader2.Close();
                myCommand.Dispose();
                connection.Close();
                string json = descripcion;
                return json;

            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        /// <summary>
        /// Método que obtiene las marcas por aseguradoras
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <returns></returns>
        public string GetMarcasMyysql(string aseguradora)
        {
           
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "select distinct(Marca) from `ws_db`.`ConcentradoPG` where aseguradora =  '" + aseguradora + "' ORDER BY MARCA;";
               
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                connection.Close();
                return json;

            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        /// <summary>
        /// Método que obtiene los modelos por asguradora y marca
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <param name="marca"></param>
        /// <returns></returns>
        public string GetModelosMySql(string aseguradora,string marca)
        {
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "select distinct(MODELO) from `ws_db`.`ConcentradoPG` where aseguradora =  '" + aseguradora + "' and MARCA = '" + marca + "' ORDER BY MODELO;";
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        /// <summary>
        /// Metodo que obtien la submarca latino
        /// </summary>
        /// <param name="Marca"></param>
        /// <returns></returns>
        public string GetSubMarcasLatinoMysql(string Marca)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011");

            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "SELECT DISTINCT(CLAVE_SUBMARCA) FROM `ws_db`.`Latino_Catalogos_LATINO_DESCRIPCIONES` WHERE CLAVE_MARCA = '" + Marca + "' order by CLAVE_SUBMARCA";
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                connection.Close();
                return json;
            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        public string GetSubMarcaMySql(string aseguradora, string marca, string modelo)
        {
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "select distinct(SUBMARCA) from `ws_db`.`ConcentradoPG` where aseguradora =  '" + aseguradora + "' and MARCA = '" + marca + "' and " +
                    "Modelo = " + modelo + " ORDER BY Marca";
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        /// <summary>
        /// Método que obtienen descripcion por marca, aseguradora
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <returns></returns>
        public string GetDescripcionesMySql(string aseguradora, string marca, string modelo, string subMarca)
        {
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "select distinct(descripcion) from `ws_db`.`ConcentradoPG` where aseguradora =  '" + aseguradora + "' and MARCA = '" + marca + "' and MODELO = '"+modelo+"' " +
                    "and SUBMARCA = '"+subMarca+"' and DESCRIPCION is not null and DESCRIPCION <> 'NULL' ORDER BY MODELO;";
                MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
                DataTable dt = new DataTable();
                adapter.SelectCommand.CommandTimeout = 0;
                adapter.Fill(dt);
                string json = JsonConvert.SerializeObject(dt);
                return json;

            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        //---------Latino Query sql Server
        /// <summary>
        /// Metodo que recupera la clave de estado
        /// </summary>
        /// <param name="Cp"></param>
        /// <returns></returns>
        public string GetEstadoLatinoMysql(string Cp)
        {
            //SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Replica_Ali_R;user id = servici1; password = Master@011");
            try
            {
                int comparadorEcatepec = 0;
                int comparadorIxtapaluca = 0;
                int comparadorCiudadMexico = 0;
                codigo_postalEstadoMexico = Cp.Substring(0, 3);
                codigo_postalEstadoMexicoDosNumeros = Cp.Substring(0, 2);
                codigo_postalExterno = Cp.Substring(0, 2);
                codigo_postalEcatepec = Cp.Substring(2, 1);
                codigo_postalChiapas = Cp.Substring(0, 2);
                codigo_postalCiudadMexico = Cp.Substring(0, 2);
                codigo_postalJalisco = Cp.Substring(0, 2);
                codigo_postalMichoacan = Cp.Substring(0, 2);
                codigo_postalNuevoLeon = Cp.Substring(0, 2);
                codigo_postalSinaloa = Cp.Substring(0, 2);
                codigo_postalTamaulipas = Cp.Substring(0, 2);
                codigo_postalIxtapaluca = Cp.Substring(2, 1);
                codigo_postaHidalgo = Cp.Substring(0, 2);
                codigo_postaBajaCalifornia = Cp.Substring(0, 3);
                codigo_postaQuintanaRoo = Cp.Substring(0, 3);
                codigo_postalSonora = Cp.Substring(0, 2);
                codigo_postalGuerrero = Cp.Substring(0, 2);
                codigo_postalVeracruz = Cp.Substring(0, 3);

                comparadorEcatepec = int.Parse(codigo_postalEcatepec);
                comparadorCiudadMexico = int.Parse(codigo_postalCiudadMexico);
                comparadorIxtapaluca = int.Parse(codigo_postalIxtapaluca);

                if (codigo_postalEstadoMexico == "557" || codigo_postalEstadoMexico == "550" || codigo_postalEstadoMexico == "568" || codigo_postalEstadoMexico == "565" || codigo_postalEstadoMexico == "527" || codigo_postalEstadoMexico == "548")
                {
                    Cp = codigo_postalEstadoMexico;
                }
                else if (codigo_postalChiapas == "29" || codigo_postalChiapas == "30")
                {
                    Cp = "30";
                }
                else if (codigo_postalEstadoMexicoDosNumeros == "55")
                {
                    Cp = "55";
                }
                else if (comparadorCiudadMexico <= 16)
                {
                    Cp = "01";
                }
                else if (codigo_postalJalisco == "47" || codigo_postalJalisco == "45")
                {
                    Cp = "49";
                }
                else if (codigo_postalMichoacan == "59" || codigo_postalMichoacan == "61")
                {
                    Cp = "58";
                }
                else if (codigo_postalNuevoLeon == "64" || codigo_postalNuevoLeon == "65")
                {
                    Cp = "67";
                }
                else if (codigo_postalSinaloa == "80")
                {
                    Cp = "81";
                }
                else if (codigo_postalSonora == "84" || codigo_postalSonora == "85")
                {
                    Cp = "83";
                }
                else if (codigo_postalTamaulipas == "88" || codigo_postalTamaulipas == "89")
                {
                    Cp = "87";
                }
                else if (codigo_postaQuintanaRoo == "775")
                {
                    Cp = "77";
                }
                else if (comparadorIxtapaluca == 5)
                {
                    Cp = "565";
                }
                else if (Cp == "56509")
                {
                    Cp = "56509";
                }
                else if (codigo_postaBajaCalifornia == "227")
                {
                    Cp = "21";
                }
                else if (codigo_postaHidalgo == "42" || codigo_postaHidalgo == "43")
                {
                    Cp = "42";
                }
                else if (codigo_postalVeracruz == "917")
                {
                    Cp = "91";
                }
                else if(codigo_postalGuerrero == "40" || codigo_postalGuerrero == "41")
                {
                    Cp = "39";
                }
                else if (comparadorEcatepec == 7)
                {
                    Cp = "550";
                }
                else
                {
                    Cp = codigo_postalExterno;
                }
                MySqlDataReader MyReader2;
                MySqlConnection connection = new MySqlConnection(connectionString);
                string queryEstado = "SELECT DISTINCT(clave_estado) FROM `ws_db`.`Latino_Catalogos_LATINO_ESTADOS` where Cp_general ='" + Cp + "'";
                MySqlCommand myCommand = new MySqlCommand(queryEstado, connection);
                connection.Open();
                myCommand.CommandTimeout = 0;
                MyReader2 = myCommand.ExecuteReader();
                while (MyReader2.Read())
                {
                    descripcion = MyReader2.GetValue(0).ToString();
                }
                MyReader2.Close();
                myCommand.Dispose();
                connection.Close();
                string json = descripcion;
                return json;


            }

            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }

        /// <summary>
        /// Método que recupera la clave descripcion de la latino
        /// </summary>
        /// <param name="aseguradora"></param>
        /// <param name="marca"></param>
        /// <param name="modelo"></param>
        /// <param name="subMarca"></param>
        /// <returns>Clave descrpción</returns>
        public string GetClaveDescripcionLatino(string clave_Marca, string clave_SubMarca, string descripcion, string año)
        {
            try
            {
                MySqlDataReader MyReader2;
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                string query = "SELECT  clave_descripcion FROM `ws_db`.`Latino_Catalogos_LATINO_DESCRIPCIONES` where CLAVE_MARCA = '" + clave_Marca + "' and CLAVE_SUBMARCA = '" + clave_SubMarca + "' and DESCRIPCION like '%" + descripcion + "%' and AÑO = '" + año + "'";
                MySqlCommand myCommand = new MySqlCommand(query, connection);
                myCommand.CommandTimeout = 0;
                MyReader2 = myCommand.ExecuteReader();
                while (MyReader2.Read())
                {
                    claveDescripcion = MyReader2.GetValue(0).ToString();
                }
                MyReader2.Close();
                myCommand.Dispose();
                connection.Close();
                string json = claveDescripcion;
                return json;


            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }

        /// <summary>
        /// Metodo que regresa submarca latino
        /// </summary>
        /// <param name="clave_Marca"></param>
        /// <param name="descripcion"></param>
        /// <param name="año"></param>
        /// <returns></returns>
        public string GetSubMarcaLatino(string clave_Marca, string año, string descripcion)
        {
            try
            {
                MySqlDataReader MyReader2;
                MySqlConnection connection = new MySqlConnection(connectionString);
                string query = "select  DISTINCT(clave_submarca) from  `ws_db`.`Latino_Catalogos_LATINO_DESCRIPCIONES` where CLAVE_MARCA ='" + clave_Marca + "' and AÑO ='" + año + "' and DESCRIPCION LIKE '%" + descripcion + "%'";
                MySqlCommand myCommand = new MySqlCommand(query, connection);
                connection.Open();
                myCommand.CommandTimeout = 0;
                MyReader2 = myCommand.ExecuteReader();
                while (MyReader2.Read())
                {
                    subMarcaLatino = MyReader2.GetValue(0).ToString();
                }
                MyReader2.Close();
                myCommand.Dispose();
                connection.Close();
                string json = subMarcaLatino;
                return json;

            }
            catch (Exception e)
            {
                return "error: " + e;
            }
        }
    }
}
