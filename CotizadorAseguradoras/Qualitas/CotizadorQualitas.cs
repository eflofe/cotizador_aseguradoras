﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace CotizadorAseguradoras.Qualitas
{
    public class CotizadorQualitas
    {
        XmlFormat xml = new XmlFormat();
        LogCotizacion lg = new LogCotizacion();
        string respuestaWS = "";
        string Negocio = "2886";
        string TipoMovimiento = "2";
        string FecIniVig = DateTime.Now.ToString("yyyy-MM-dd");
        string FecFinVig = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd");
        string Paquete = "";
        string Agente = "71315";
        string FormaPago = "";
        string Tarifa = "LINEA";
        string DigitoAmis;
        string uso = "";
        string servicio = "";

        public string CotizaQualitas(Seguro seguro, string log)
        {
            string CveAmis = seguro.Vehiculo.Clave.ToString();

            if (CveAmis.Length == 4)
            {
                CveAmis = "0" + CveAmis;
                DigitoAmis = CveAmis;
            }
            else if (CveAmis.Length == 3)
            {
                CveAmis = "00" + CveAmis;
                DigitoAmis = CveAmis;
            }
            else
            {
                DigitoAmis = CveAmis;
            }
            if (seguro.Paquete == "AMPLIA")
            {
                Paquete = "01";
            }
            else if (seguro.Paquete == "LIMITADA")
            {
                Paquete = "03";
            }
            if (seguro.Vehiculo.Uso == "PARTICULAR")
            {
                uso = "01";
            }
            if (seguro.Vehiculo.Servicio == "PARTICULAR")
            {
                servicio = "01";
            }
            string validador = DigitoValidador(DigitoAmis);
            string idEdo = lg.getEstado(seguro.Cliente.Direccion.CodPostal);

            var xmlCotizacion = xml.GenerateXml(seguro, uso, Paquete, servicio, seguro.Vehiculo.Clave.ToString(), validador, idEdo);
            lg.UpdateLogRequest(xmlCotizacion, log);
            WsCotizaQ.wsEmisionServiceSoapClient q = new WsCotizaQ.wsEmisionServiceSoapClient();
            respuestaWS = q.obtenerNuevaEmision(xmlCotizacion);
            lg.UpdateLogResponse(respuestaWS, log);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(respuestaWS);
            string json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
            JsonRespuestaQualitas JsonQ = new JsonRespuestaQualitas();
            string jsonResponse = JsonQ.JsonQualitasRespuesta(json, seguro);
            lg.UpdateLogJsonResponse(jsonResponse, log);

            return jsonResponse;
        }

        public string DigitoValidador(string clave)
        {
            string ami = clave;
            string impar1 = ami.Substring(0, 1);
            string impar2 = ami.Substring(2, 1);
            string impar3 = ami.Substring(4, 1);
            string par1 = ami.Substring(1, 1);
            string par2 = ami.Substring(3, 1);
            int sumaTotal = ((Convert.ToInt32(impar1) + Convert.ToInt32(impar2) + Convert.ToInt32(impar3)) * 3) + (Convert.ToInt32(par1) + Convert.ToInt32(par2));
            int r = 10 - (sumaTotal % 10);
            return r.ToString();
        }
    }
}